### Beta-Version v.01 - "Pączek w maśle" - 2024-02-10-17-30
- Obsługa dodawania recenzji
- Obsługa modyfikowania recenzji
- Obsługa przeglądania progresji
- Logowanie i rejestracja użytkowników

### Beta-Version v.02 - "withoutstory" - 2024-03-05-21-26
- Naprawiono problem związany z logowaniem się e-mail'em zawierającym znaki specjalne
- Naprawiono wyświetlanie oceny "0" dla gier, które nie dostały oceny za fabułę